defmodule Mix.Tasks.PngInfo do
  use Mix.Task
  @moduledoc false

  @impl Mix.Task
  def run([file]) do
    data = File.read!(file)
    out = PngInfo.parse(data)
    IO.inspect(out)
  end
end
