defmodule PngInfo do
  @moduledoc """
  Extracts `image/png` basic information:

  * width, height
  * color type
  * uses alpha
  * interlace, compression, filter and bit depth

  This module can work with partial/streamed binary data, as it does not requires the whole file. Depending on the PNG file,
  the module may need from 33 bytes to more. The `stream/1` function initiates a parser, while its return `{:continue, ...}`
  must be used with `stream/2` with more bytes.
  Once enough bytes has been supplied and the information parsed, `{:ok, t()}` will be returned.

  """

  defstruct [:width, :height, :bit_depth, :filter, :color_type, :alpha, :compression, :interlace]

  @type t :: %__MODULE__{
          width: non_neg_integer(),
          height: non_neg_integer(),
          bit_depth: 1 | 2 | 4 | 8 | 16,
          filter: false | :adaptive | non_neg_integer(),
          color_type: :greyscale | :truecolour | :indexed,
          alpha: boolean(),
          compression: :deflate | non_neg_integer(),
          interlace: false | :adam7 | non_neg_integer()
        }
  @type stream_return ::
          {:ok, t()} | {:continue, continuation()} | {:error, {:invalid_png, error_reason()}}
  @opaque continuation :: {function :: atom(), args :: list(), acc :: any()}

  @color_types %{
    0 => {:greyscale, :maybe},
    2 => {:truecolour, :maybe},
    3 => {:indexed, :maybe},
    4 => {:greyscale, true},
    6 => {:truecolour, true}
  }

  @type error_reason ::
          :bad_magic
          | :header_checksum_mismatch
          | :trns_checksum_mistmatch
          | :missing_header
          | :incomplete_data
  @spec parse(binary()) :: {:ok, t()} | {:error, {:invalid_png, error_reason()}}
  @doc "Extracts `t:t()` from a complete PNG `binary`."
  def parse(binary) do
    case stream(binary) do
      {:ok, _} = ok -> ok
      {:error, _} = error -> error
      {:continue, _} -> {:error, {:invalid_png, :incomplete_data}}
    end
  end

  @spec stream(binary()) :: stream_return()
  @doc "Starts a stream"
  def stream(binary \\ <<>>) do
    stream({:find_magic, [], <<>>}, binary)
  end

  @spec stream(continuation(), binary()) :: stream_return()
  @doc "Continue a stream"
  def stream(continuation, binary)

  def stream({fun, _, <<>>}, <<>>) when fun != :find_magic do
    {:error, {:invalid_png, :incomplete_data}}
  end

  def stream({fun, args, acc}, data) when is_atom(fun) and is_list(args) do
    data = [acc <> data] ++ args

    case :erlang.apply(__MODULE__, fun, data) do
      {:continue, cont} -> {:continue, cont}
      other -> other
    end
  end

  @doc false
  def find_magic(<<137, 80, 78, 71, 13, 10, 26, 10, rest::bits>>) do
    find_chunk(rest, %__MODULE__{}, "IHDR")
  end

  def find_magic(<<_, _, _, _, _, _, _, _, _rest::bits>>) do
    {:error, {:invalid_png, :bad_magic}}
  end

  def find_magic(binary) do
    {:continue, {:find_magic, [], binary}}
  end

  # Find a chunk
  @doc false
  def find_chunk(<<size::integer-size(4)-unit(8), rest::bits>>, info, name) do
    find_chunk_name(rest, info, name, size)
  end

  def find_chunk(<<rest::bits>>, info, name), do: {:continue, {:find_chunk, [info, name], rest}}

  # CHUNK NAME

  # tRNS must be before IDAT
  @doc false
  def find_chunk_name(<<"IDAT", _::bits>>, info, "tRNS", _size) do
    {:ok, %__MODULE__{info | alpha: false}}
  end

  # Header must be the first chunk
  def find_chunk_name(<<chunk_name::binary-size(4), _::bits>>, _, "IHDR", _)
      when chunk_name != "IHDR" do
    {:error, {:invalid_png, :missing_header}}
  end

  def find_chunk_name(<<chunk_name::binary-size(4), rest::bits>>, info, name, size) do
    find_chunk_data(rest, info, name, size, name == chunk_name)
  end

  def find_chunk_name(<<rest::bits>>, info, name, size) do
    {:continue, {:find_chunk_name, [info, name, size], rest}}
  end

  # CHUNK DATA
  @doc false
  def find_chunk_data(rest, info, name, size, found) do
    case rest do
      <<data::binary-size(size), rest::bits>> ->
        find_chunk_crc(rest, info, name, size, found, data)

      _ ->
        {:continue, {:find_chunk_data, [info, name, size, found], rest}}
    end
  end

  # CHUNK CRC

  # Verify CRC if this is the chunk we wanted
  @doc false
  def find_chunk_crc(
        <<crc::integer-size(4)-unit(8), rest::bits>>,
        info,
        "IHDR",
        _size,
        true,
        data
      ) do
    parse_header(rest, info, {"IHDR", data, check_crc("IHDR", data, crc)})
  end

  # Verify CRC if this is the chunk we wanted, and return
  def find_chunk_crc(
        <<crc::integer-size(4)-unit(8), _rest::bits>>,
        info,
        "tRNS",
        _size,
        true,
        data
      ) do
    if check_crc("tRNS", data, crc) do
      {:ok, %__MODULE__{info | alpha: true}}
    else
      {:error, :trns_checksum_mistmatch}
    end
  end

  def find_chunk_crc(
        <<_crc::integer-size(4)-unit(8), rest::bits>>,
        info,
        name,
        _size,
        _found,
        _data
      ) do
    find_chunk(rest, info, name)
  end

  def find_chunk_crc(rest, info, name, size, found, data) do
    {:continue, {:find_chunk_crc, [info, name, size, found, data], rest}}
  end

  # Parse IHDR data
  defp parse_header(
         <<rest::bits>>,
         info,
         {"IHDR",
          <<width::integer-size(4)-unit(8), height::integer-size(4)-unit(8),
            depth::integer-size(8), color_type::integer-size(8), filter::integer-size(8),
            compression::integer-size(8), interlace::integer-size(8)>>, true}
       ) do
    {color_type, alpha} = Map.get(@color_types, color_type)

    compression =
      case compression do
        0 -> :deflate
      end

    interlace =
      case interlace do
        0 -> false
        1 -> :adam7
        i -> i
      end

    filter =
      case filter do
        0 -> :adaptive
        i -> i
      end

    info = %__MODULE__{
      info
      | width: width,
        height: height,
        bit_depth: depth,
        filter: filter,
        color_type: color_type,
        alpha: alpha,
        compression: compression,
        interlace: interlace
    }

    if alpha == :maybe do
      find_chunk(rest, info, "tRNS")
    else
      {:ok, info}
    end
  end

  defp parse_header(_, _, {"IHDR", _, false}) do
    {:error, {:invalid_png, :header_checksum_mismatch}}
  end

  defp parse_header(_, _, _) do
    {:error, {:invalid_png, :header_missing}}
  end

  defp check_crc(name, data, crc) do
    name
    |> :erlang.crc32()
    |> :erlang.crc32(data)
    |> Kernel.==(crc)
  end
end
