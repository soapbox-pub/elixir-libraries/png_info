defmodule PngInfoTest do
  use ExUnit.Case
  doctest PngInfo

  test "indexed" do
    assert {:ok, info} = fake_stream("indexed_noalpha.png")
    assert {:ok, ^info} = PngInfo.parse(image("indexed_noalpha.png"))
    assert info.alpha == false
    assert info.color_type == :indexed
    assert {info.height, info.width} == {32, 32}
    assert info.bit_depth == 4
    assert info.compression == :deflate
    assert info.filter == :adaptive
    assert info.interlace == false
  end

  test "indexed, transparency" do
    assert {:ok, info} = fake_stream("indexed_trns_null.png")
    assert {:ok, ^info} = PngInfo.parse(image("indexed_trns_null.png"))
    assert info.alpha == true
    assert info.color_type == :indexed
    assert {info.height, info.width} == {32, 32}
    assert info.bit_depth == 8
  end

  test "greyscale, alpha" do
    assert {:ok, info} = fake_stream("test_greyscale_alpha.png")
    assert {:ok, ^info} = PngInfo.parse(image("test_greyscale_alpha.png"))
    assert info.alpha == true
    assert info.color_type == :greyscale
    assert {info.height, info.width} == {32, 32}
  end

  test "truecolour, no transparency" do
    assert {:ok, info} = fake_stream("truecolour_notrns.png")
    assert {:ok, ^info} = PngInfo.parse(image("truecolour_notrns.png"))
    assert info.alpha == false
    assert info.color_type == :truecolour
  end

  test "greyscale, transparency" do
    assert {:ok, info} = fake_stream("greyscale_trns.png")
    assert {:ok, ^info} = PngInfo.parse(image("greyscale_trns.png"))
    assert info.alpha == true
  end

  test "corrupted, empty greyscale" do
    assert {:error, {:invalid_png, _}} = fake_stream("corrupted_empty.png")
    assert {:error, {:invalid_png, _}} = PngInfo.parse(image("corrupted_empty.png"))
  end

  test "corrupted, added cr bytes" do
    assert {:error, {:invalid_png, _}} = fake_stream("corrupted_added_cr_bytes.png")
    assert {:error, {:invalid_png, _}} = PngInfo.parse(image("corrupted_added_cr_bytes.png"))
  end

  test "corrupted, cr>lf, null removed" do
    assert {:error, {:invalid_png, _}} = fake_stream("corrupted_crlf_null.png")
    assert {:error, {:invalid_png, _}} = PngInfo.parse(image("corrupted_crlf_null.png"))
  end

  defp image(name) do
    File.read!("test/images/#{name}")
  end

  defp fake_stream(image, chunk_size \\ 2) do
    image = image(image)
    {:continue, cont} = PngInfo.stream(<<>>)
    fake_chunk(image, chunk_size, cont)
  end

  defp fake_chunk(binary, size, cont) do
    s = size * 8

    {data, rest} =
      case binary do
        <<data::bits-size(s), rest::bits>> -> {data, rest}
        <<data::bits>> -> {data, <<>>}
      end

    case PngInfo.stream(cont, data) do
      {:continue, cont} ->
        fake_chunk(rest, size, cont)

      other ->
        other
    end
  end
end
